
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// Webpack CSS import
import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bulma-start/css/main.css'; 

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
