import React, { Component } from 'react'
import { Card  } from 'antd'
import {Col} from 'reactstrap'
const { Meta } = Card;

export class Cardss extends Component {
  
  render() {
    return (
      <Col>
          <Card
          hoverable
          style={{ width: 240 }}
          cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
          >
          <Meta
            title="Europe Street beat"
            description="www.instagram.com"
          />
        </Card>
        <br/>
      </Col>
    )
  }
}

export default Cardss
