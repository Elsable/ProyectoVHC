import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Avatar,Select ,DatePicker} from 'antd';
import swal from 'sweetalert';

const Option = Select.Option;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

function handleChange(value) {
  console.log(`selected ${value}`);
}

function handleBlur() {
  console.log('blur');
}

function handleFocus() {
  console.log('focus');
}
function onChange(date, dateString) {
  console.log(date, dateString);
}

export class Perfil extends Component {
GuardarCambios(){
  swal("Datos Guardados!", "Usted guardo sus datos!", "success");

  
}
  render() {
    return (
      <div className="col-md-12">
      <Form className="container jumbotron">

      <center>
        <Avatar size={195} icon="user" />
      </center>
      <FormGroup>
          <Label for="username">User Name</Label>
          <Input type="text" name="username"  placeholder="User Name" />
        </FormGroup>
        <FormGroup>
          <Label for="Nombre">Nombre(s)</Label>
          <Input type="text" name="Nombre"  placeholder="Nombre(s)" />
        </FormGroup>
        <FormGroup>
          <Label for="Apellidos">Apellidos</Label>
          <Input type="text" name="Apellidos"  placeholder="Apellidos" />
        </FormGroup>

        <FormGroup>
          <Label for="Genero">Genero</Label>
          <Select
    showSearch
    
    placeholder="Select un genero"
    optionFilterProp="children"
    onChange={handleChange}
    onFocus={handleFocus}
    onBlur={handleBlur}
    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
  >
    <Option value="Masculino">Masculino</Option>
    <Option value="Femenino">Femenino</Option>
    
  </Select>
  
        </FormGroup>
        <FormGroup>
          <Label for="exampleDate">Fecha de Nacimiento</Label>
          <DatePicker onChange={onChange}  className="col-md-12"/>
        </FormGroup>
        <Button onClick={this.GuardarCambios}>Guardar cambios</Button>
      </Form>
    </div>
    )
  }
}

export default Perfil
