import React, { Component } from 'react'
import logo from './../../../../logo.svg';
import { Link } from 'react-router-dom';

export class SobreApp extends Component {

    render() {
        return (
            <div className="container jumbotron" style={{ 'text-align': "center" }}>
                <div>
                    <img src={logo} className="App-logo" alt="logo" />
                </div>
                <h2> <strong>Versión <br />1.0.2<br /></strong></h2>
                <h1> Esta Aplicacion se construyo con:</h1>
                <h3><br /><strong>Golang</strong>
                    <br /><h5>
                        <Link to=''> Página de inicio</Link> - <Link to=''>Mostrar licencia</Link>
                        </h5>
                </h3>
                <h3><br /><strong>Node</strong>
                    <br /><h5>
                        <Link to=''> Página de inicio</Link> - <Link to=''>Mostrar licencia</Link>
                        </h5>
                </h3>
                <h3><br /><strong>Rails</strong>
                    <br /><h5>
                        <Link to=''> Página de inicio</Link> - <Link to=''>Mostrar licencia</Link>
                        </h5>
                </h3>
                <h3><br /><strong>MongoDB</strong>
                    <br /><h5>
                        <Link to=''> Página de inicio</Link> - <Link to=''>Mostrar licencia</Link>
                        </h5>
                </h3>
                <h3><br /><strong>React</strong>
                    <br /><h5>
                        <Link to=''> Página de inicio</Link> - <Link to=''>Mostrar licencia</Link>
                        </h5>
                </h3>
                <h2> <strong>Realizado por:<br />Luis Antonio Padre Garcia y Jose Antonio Padre Garcia<br /></strong></h2>
                
            </div>
        )
    }
}

export default SobreApp
