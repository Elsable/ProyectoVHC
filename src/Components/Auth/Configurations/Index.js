import React, { Component } from 'react'

import { Tabs } from 'antd';
import { SobreApp } from './SobreApp/Index';
import { Perfil } from './Perfil/Index';

const TabPane = Tabs.TabPane;

function callback(key) {
  console.log(key);
}

export class Configuraciones extends Component {

  render() {
    return (
      <div>
        <Tabs onChange={callback} type="card">
          <TabPane tab="Perfil" key="1"><Perfil/></TabPane>
          <TabPane tab="Tu cuenta" key="2">Content of Tab Pane 2</TabPane>
          <TabPane tab="Sobre La Applicacion Web" key="3"><SobreApp/></TabPane>
        </Tabs>,
      </div>
    )
  }
}

export default Configuraciones
