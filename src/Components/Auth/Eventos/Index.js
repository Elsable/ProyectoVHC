import React, { Component } from 'react'
import { Buscador } from './buscador';
import { MenEventos } from './Menu';


export class Eventos extends Component {

  render() {
    return (
      <div >
        <div style={{ background: "#FFF" }} className="row">
          <div className="col-md-3">
            <MenEventos />
          </div>
          <div className="col-md-9">
            <h1 className="subtitle is-1">Eventos de ultima hora</h1>
            <hr />
            <div style={{ paddingBottom: "2em" }}>

            </div>
            <Buscador />
          </div>

        </div>
      </div>
    )
  }
}

export default Eventos
