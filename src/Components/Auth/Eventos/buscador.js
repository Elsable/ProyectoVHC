import React, { Component } from 'react'

export class Buscador extends Component {
  render() {
    return (
      <div style={{paddingBottom:"2em"}}  >          
            <div className="box">
                <div className="field has-addons">
                    <div className="control is-expanded">
                        <input className="input has-text-centered" type="search" placeholder="» » » » » » Buscar Eventos « « « « « «"/>
                    </div>
                <div className="control">
                    <a className="button is-info">Search</a>
                </div>
            </div>
            
      </div>
        
      </div>
    )
  }
}

export default Buscador
