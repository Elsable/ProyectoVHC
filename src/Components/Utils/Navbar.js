import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { Menu, Icon,Badge } from 'antd';


const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

export class Navbar extends Component {
  state = {
    current: 'mail',
  }

  handleClick = (e) => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  }

  render() {
    return (
      <div>


        <header className="header is-global navbar navbar-expand-lg fixed-top ">
          <div className="is-navbar-container">
            <div className="is-brand">
              <a href="/" className="is-logo">Imperavi</a>
              <a href="#" className="nav-toggle is-push-right-mobile is-shown-mobile icon-kube-menu" data-kube="toggle" data-target="#navbar"></a>
            </div>
            <nav id="navbar" className="is-navbar is-push-right is-hidden-mobile">
              <ul>
              <li>
                  <Link to="/Trabajos">Trabajos</Link>
                </li>
                <li>
                  <Badge count={5}>
                    <Link to="/Noticias">Noticias</Link>
                  </Badge>
                </li>
                
                <li>
                  <Link to="/Personas/">Personas</Link>
                </li>
                <li>
                  < Link to="/Eventos/">Eventos</Link>
                </li>
                <li className="is-parent">
                
                  <Link to="/Notificaciones/">Notificaciones</Link>
                </li>
                <li>
                  {/* <Link to="/account/">My Account</Link> */}
                  <Menu
                    onClick={this.handleClick}
                    selectedKeys={[this.state.current]}
                    mode="horizontal"
                    theme="dark"
                  >
                    <SubMenu title={<span><Icon type="setting" />{'Luis Antonio Padre Garcia'}</span>}>
                      <MenuItemGroup title="">
                      <Menu.Item key="setting:1"><Link to="/perfil">Perfil</Link></Menu.Item>
                        <Menu.Item key="setting:2"><Link to="/Configuraciones">Configuraciones</Link></Menu.Item>
                        <Menu.Item key="setting:3"><Link to="/">Cerrar Sesión</Link></Menu.Item>
                      </MenuItemGroup>
                      <MenuItemGroup title="">
                      <Menu.Item key="setting:4"><Link to="/Ayuda">Ayuda</Link></Menu.Item>
                      <Menu.Item key="setting:5"><Link to="/sugerencias">Enviar sugerencias</Link></Menu.Item>
                      <Menu.Item key="setting:6">Idioma:Español</Menu.Item>
                      </MenuItemGroup>
                    </SubMenu>
                  </Menu>
                </li>
              </ul>
            </nav>
          </div>
        </header>
      </div>
    )
  }
}

export default Navbar
