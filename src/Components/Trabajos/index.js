import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { Layout,  Breadcrumb } from 'antd';
const { Content } = Layout;


export class Index extends Component {
  

  render() {
    return (
      <div className="container">
      <Content style={{ padding: '0 50px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <center>
          <h5 style={{color:444}}>
            
            <Link to="/Send_Curriculum">Publica tu CV</Link> - Postúlate a miles de empleos desde cualquier dispositivo<br/>
            <Link to="/Publish_Work">Publica un empleo</Link> - Su próximo empleado está aquí
            
            </h5>
        </center>
        </Breadcrumb>
        </Content>
      </div>
    )
  }
}

export default Index
