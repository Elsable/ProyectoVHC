import React, { Component } from 'react'
import swal from 'sweetalert';
import {Button} from 'antd'

export class Paso4FT extends Component {
    
    SubirEmpleo(){
        swal("¡Empleo Guardado!", "Usted a subido su Empleo al portal!", "success");    
        
    }
    
  render() {
    return (
      <div className="content">
      

            <hr/>
                <center>                                     
                    <Button type="primary" onClick={() => this.setModal1Visible(true)} >Vista Previa</Button>                        
                    <Button type="primary" icon="download" onClick={this.SubirEmpleo} >Publicar Empleo</Button>
                </center>
            <hr/>
            
      </div>
    )
  }
}

export default Paso4FT
