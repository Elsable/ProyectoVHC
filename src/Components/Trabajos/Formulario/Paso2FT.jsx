import React, { Component } from 'react'
import {InputNumber, Select, Divider} from 'antd'
import { FormGroup, Label } from 'reactstrap';
const Option = Select.Option;
function handleChange(value) {
  console.log(`selected ${value}`);
}
function onChange(value) {
    console.log('changed', value);
}
function handleBlur() {
    console.log('blur');
  }
  
  function handleFocus() {
    console.log('focus');
  }
export class Paso2FT extends Component {
  

  render() {
    return (
      <div>
        <FormGroup>
            <Label for="Salario">¿Cuál es el salario para este empleo? </Label>
            <br/>
            <InputNumber
                defaultValue={3000}
                formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                onChange={onChange}
                
            />
             {''} a {''}
            <InputNumber
                defaultValue={10000}
                formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                onChange={onChange}
            />
            {''} {''}
            <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Seleccione la forma del pago"
                optionFilterProp="children"
                onChange={handleChange}
                onFocus={handleFocus}
                onBlur={handleBlur}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
                
                <Option value="hora">Por Hora</Option>
                <Option value="dia">Al Dia</Option>                
                <Option value="semana">A la Semana</Option>
                <Option value="quincena">A la Quincena</Option>
                <Option value="mes">Al mes</Option>
                <Option value="año">Al año</Option>
            </Select>
        </FormGroup>
        <hr/>
        <h4 className="title is-4">Descripción del Empleo</h4>
        <FormGroup>
        Describa las responsabilidades y los requisitos necesarios del empleo como habilidades, nivel de educación y experiencia laboral.
        <div className="media-content">
          <div className="field">
            <p className="control">
              <textarea className="textarea" placeholder="Agregar descripción de Empleo..."></textarea>
            </p>
          </div>
        </div>
        La descripción del empleo no deberá violar la "Ley Federal para Prevenir y Eliminar la Discriminación", la cual incluye discriminación de sexo, edad, origen, raza, color, estado civil, entre otros.
        </FormGroup>
      </div>
    )
  }
}

export default Paso2FT
