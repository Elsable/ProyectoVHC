import React, { Component } from 'react'
import { Radio } from 'antd';
import { FormGroup, Label, Input } from 'reactstrap';

export class Paso1FT extends Component {


  render() {
    return (
      <div>
        <FormGroup>
            <Label for="Titulo">Titulo del Empleo: </Label>
            <Input type="text" name="Titulo" placeholder="Titulo breve y especifico." />
        </FormGroup>
        <FormGroup>
            <Label for="Empresa">Nombre de la Empresa: </Label>
            <Input type="text" name="Empresa" placeholder="Escriba el nombre de su empresa." />
        </FormGroup>
        <FormGroup>
            <Label for="Ubicacion">Ubicación: </Label>
            <Input type="text" name="Ubicacion" placeholder="Ciudad." />
        </FormGroup>


        <FormGroup>
        <Label for="Contrato">¿Cuál es el tipo de contrato para este empleo? </Label>
        <br/>
        <center>
        <Radio.Group buttonStyle="solid" size="large" >
            <Radio.Button value="Tiempo_Completo">Tiempo Completo</Radio.Button>
            <Radio.Button value="Medio_Tiempo">Medio Tiempo</Radio.Button>
            <Radio.Button value="Indefinido">Indefinido</Radio.Button>
            <Radio.Button value="Temporal">Temporal</Radio.Button>
            <Radio.Button value="Contrato">Contrato</Radio.Button>
            <Radio.Button value="Comision">Comisión</Radio.Button>
            <Radio.Button value="Becas_Practicas">Becas/Prácticas</Radio.Button>
        </Radio.Group>
        </center>
        </FormGroup>
      </div>
    )
  }
}

export default Paso1FT
