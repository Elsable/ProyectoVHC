import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FormGroup, Label, Input } from 'reactstrap';
export class Paso3FT extends Component {
  static propTypes = {

  }

  render() {
    return (
      <div>
        <br/>
        <FormGroup>
        <h5 className="subtitle is-5"><Label for="Beneficios">Beneficios: </Label></h5>
            <Input type="text" name="Beneficios" placeholder="Remuneraciones NO IMPONIBLES como Transporte, Colación, Bonos, Seguros, etc " />
        </FormGroup>
        <FormGroup>
        <h5 className="subtitle is-5"><Label for="Ambiente">Ambiente Laboral: </Label></h5>
            <Input type="text" name="Ambiente" placeholder=" ¿Como es Trabajar en esta empresa? " />
        </FormGroup>
        <FormGroup>
          <h5 className="subtitle is-5"><Label for="Beneficios">Seleccione una imagen: </Label></h5>
          La imagen debe representar a la empresa o las actividades a realizar
          <div className="file">
            <label className="file-label">
              <input className="file-input" type="file" name="resume"/>
              <span className="file-cta">
                <span className="file-icon">
                  <i classNames="fas fa-upload"></i>
                </span>
                <span className="file-label">
                  Choose a file…
                </span>
              </span>
            </label>
          </div>
        </FormGroup>
      </div>
    )
  }
}

export default Paso3FT
