import React, { Component } from 'react'
import { Row, Col, Steps, Button, message } from 'antd';
import {  Form  } from 'reactstrap';
import { Paso1FT } from './Formulario/Paso1FT';
import { Paso2FT } from './Formulario/Paso2FT';
import { Paso3FT } from './Formulario/Paso3FT';
import { Paso4FT } from './Formulario/Paso4FT';

const Step = Steps.Step;
const steps = [{
  title: 'Paso 1',
  content: <Paso1FT/>
}, {
  title: 'Paso 2',
  content: <Paso2FT/>
}, {
  title: 'Paso 3',
  content: <Paso3FT/>
}, {
  title: 'Paso 4',
  content: <Paso4FT/>,
}];


export class Publish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
    };
  }
  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  
  render() {    
    const { current } = this.state;
    return (
      <div className="container jumbotron">
        <center>
          <h2>Crea un Empleo</h2>
        </center>
        <Form>                    
          <Steps current={current}>
            {steps.map(item => <Step key={item.title} title={item.title} />)}
          </Steps>
        <div className="steps-content">{steps[current].content}</div>
          <div className="steps-action">         
          
          
          
            {
              current < steps.length - 1
              && 
              <Button type="primary" onClick={() => this.next()}>Next</Button>
              
            }
                        
            {
              current > 0
              && (                
                <Button onClick={() => this.prev()} >
                  Previous
                </Button>
              )
            }
            
          
          </div>

          
          
          </Form>
        
      </div>
    )
  }
}

export default Publish
