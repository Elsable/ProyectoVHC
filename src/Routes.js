import React, {
    Component
} from 'react'
import { Navbar } from './Components/Utils/Navbar';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './Components/Auth/Home';
import Perfil from './Components/Auth/Profile/Index';
import Works from './Components/Trabajos/index'
import Publicacion from './Components/Trabajos/Publish'
import { Dashboard } from './Components/Auth/Dashboard/Index';
import { Eventos } from './Components/Auth/Eventos/Index';
import { Personas } from './Components/Auth/Personas/index';
import { Noticias } from './Components/Auth/Noticias/Index';
import { Configuraciones } from './Components/Auth/Configurations/Index';

export class Routes extends Component {


    render() {
        return (<div >
            <Router>
                <div>
                    
                    <Navbar />
                    <div className="col-md-12">
                        <Route exact path="/" component={Home} />
                        <Route path="/about" component={Home} />
                        <Route path="/Trabajos" component={Works} />
                        <Route path="/perfil" component={Perfil} />
                        <Route path="/Publish_Work" component={Publicacion} />
                        <Route path="/Dashboard" component={Dashboard} />
                        <Route path="/Eventos" component={Eventos} />
                        <Route path="/Personas" component={Personas} />
                        <Route path="/Noticias" component={Noticias} />
                        <Route path="/Configuraciones" component={Configuraciones} />
                        
                        
                    </div>
                    
                </div>
            </Router>

        </div>
        )
    }
}

export default Routes